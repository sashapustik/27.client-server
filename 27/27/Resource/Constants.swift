import Foundation
import UIKit

struct Constants {
    static let defaultCity = "Brest"
    static let apiKey = "4af57dbc2efcd98de9d061ff5fedc9be"
    static let spaceReplace = "%20"
    
    static let kelvin = 273.15
    static let celciusTitle = "°C"
    struct AlertTitles {
        static let cancel = "Cancel"
        static let title = "Search"
        static let message = "Search city and watch weather"
        static let textFieldPlaceholder = "City"
        static let okey = "Okey"
        
    }
    
    struct HttpPart {
        static let url = "https://api.openweathermap.org/data/2.5/weather?"
        static let apiKeyPart = "&appid="
        static let imageUrl = "http://openweathermap.org/img/w/"
        static let imageExtension = ".png"
    }
    
    struct ButtonSetup {
        static let cornerRadius: CGFloat = 15
        static let shadowOffset: CGFloat = 1
        static let shadowOpacity: Float = 0.3
    }
    
    struct ImageViewSetup {
        static let shadowOffset: CGFloat = 1
        static let shadowOpacity: Float = 0.3
    }
}
