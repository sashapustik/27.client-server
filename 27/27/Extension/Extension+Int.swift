import Foundation

extension Int {
    var celsius: Int {
        return self - Int(Constants.kelvin)
    }
}
