import UIKit
import CoreLocation

class ViewController: UIViewController {

    @IBOutlet private var cityNameLabel: UILabel!
    @IBOutlet private var imageView: UIImageView!
    @IBOutlet private var temperatureLabel: UILabel!
    @IBOutlet private var temperatureFeelLikesLabel: UILabel!
    @IBOutlet private var searchButton: UIButton!
    @IBOutlet private var feelsLikeLabel: UILabel!
    @IBOutlet private var celciusLabel: UILabel!
    
    private var networkService = NetworkService()
    
    private lazy var locationManager: CLLocationManager = {
        let locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        return locationManager
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchButtonSetup()
        imageViewLayerSetup()
        networkService.delegate = self
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestLocation()
        }
        networkService.fetchWeather(by: Constants.defaultCity)
    }
    
    private func searchButtonSetup() {
        searchButton.addTarget(
            self,
            action: #selector(searchCity),
            for: .touchUpInside
        )
        searchButton.layer.cornerRadius = Constants.ButtonSetup.cornerRadius
        searchButton.layer.shadowOpacity = Constants.ButtonSetup.shadowOpacity
        searchButton.layer.shadowOffset = .init(
            width: Constants.ButtonSetup.shadowOffset,
            height: Constants.ButtonSetup.shadowOffset
        )
        
    }
    
    private func imageViewLayerSetup() {
        imageView.layer.shadowOpacity = Constants.ImageViewSetup.shadowOpacity
        imageView.layer.shadowOffset = .init(
            width: Constants.ImageViewSetup.shadowOffset,
            height: Constants.ImageViewSetup.shadowOffset
        )
        imageView.layer.borderColor = UIColor.lightGray.cgColor
    }
    //MARK: - Handlers
    @objc
    private func searchCity() {
        setCityWeather()
    }
}

extension ViewController {
    private func setCityWeather() {
        let alertController = UIAlertController(
            title: Constants.AlertTitles.title,
            message: Constants.AlertTitles.message,
            preferredStyle: .alert
        )
        alertController.addTextField { (textField) in
            textField.placeholder = Constants.AlertTitles.textFieldPlaceholder
        }
        let okeyAction = UIAlertAction(
            title: Constants.AlertTitles.okey,
            style: .default
        ) { _ in
            guard let city = alertController.textFields?.first?.text else { return }
            if city.trimmingCharacters(in: .whitespacesAndNewlines) != String() {
                self.networkService.fetchWeather(by: city)
            }
        }
        let cancelAction = UIAlertAction(
            title: Constants.AlertTitles.cancel,
            style: .cancel,
            handler: nil
        )
        alertController.view.tintColor = .black
        alertController.addAction(okeyAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension ViewController: NetworkServiceProtocol {
    func updateInterface(_: NetworkService, weather: Weather) {
        celciusLabel.isHidden = false
        feelsLikeLabel.isHidden = false
        guard let imageName = weather.weather.last?.icon else { return }
        let data = networkService.fetchImageData(by: imageName)
        imageView.image = UIImage(data: data)
        cityNameLabel.text = weather.name
        temperatureLabel.text = "\(Int(weather.main.temp).celsius)"
        temperatureFeelLikesLabel.text = "\(Int(weather.main.feelsLike).celsius) \(Constants.celciusTitle)"
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        networkService.fetchWeather(by: location.coordinate)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}
