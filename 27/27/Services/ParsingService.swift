import Foundation

struct ParsingService {

    func parsingData(by data: Data) -> Weather? {
        let weather = try? JSONDecoder().decode(Weather.self, from: data)
        return weather
    }
}
