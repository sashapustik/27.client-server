import Foundation
import CoreLocation

protocol NetworkServiceProtocol: class {
    func updateInterface(_: NetworkService, weather: Weather)
}

class NetworkService {
    private let parsingService = ParsingService()
    
    weak var delegate: NetworkServiceProtocol?
    
    func fetchWeather(by city: String) {
        guard
            let url = URL(
                string: "\(Constants.HttpPart.url)q=\(city.replacingOccurrences(of: " ", with: Constants.spaceReplace))\(Constants.HttpPart.apiKeyPart)\(Constants.apiKey)"
            )
        else { return }
        performSession(by: url)
    }
    
    func fetchWeather(by coordinate: CLLocationCoordinate2D) {
        let coordinate = "\(Constants.HttpPart.url)lat=\(coordinate.latitude)&lon=\(coordinate.longitude)"
        guard let url = URL(string: "\(coordinate)&appid=\(Constants.apiKey)&units=metric") else { return }
        performSession(by: url)
    }
    
    private func performSession(by url: URL) {
        let session = URLSession.shared
        session.dataTask(with: url) { [weak self] (data, response, error) in
            guard let self = self else { return }
            if error == nil {
                guard
                    let data = data,
                    let weather = self.parsingService.parsingData(by: data)
                else { return }
                DispatchQueue.main.async {
                    self.delegate?.updateInterface(self, weather: weather)
                }
            } else {
                guard let error = error else { return }
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func fetchImageData(by name: String) -> Data {
        guard
            let url = URL(string: "\(Constants.HttpPart.imageUrl)\(name)\(Constants.HttpPart.imageExtension)"),
            let data = try? Data(contentsOf: url)
        else { return Data() }
        return data
    }
}
